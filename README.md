<p align="center">
  <a href="https://layui.github.io/">
    <img src="https://plumgpt.com/chat/gpt.jpg" width="81" alt="FACE-UI">
  </a>
</p>
<h1 align="center">PLUM-GPT</h1>

**史上最强GPT它1:1还原chatgpt官网界面的网站AI对话网站，**
还原度超过90%，支持流式输出，支持markdwon语法， 支持各种代码编写等等，不仅支持AI对话，同时还有多款丰富的模型，如识别模型，可让AI识别图片的场景并跟你进行交互， plum-gpt还支持AI绘画，AI绘画室可分割为3种绘画室，公共的绘画室，单独的绘画室，和朋友的绘画室

### 项目亮点

- 支持AI对话
- 支持手机端和PC端的语音转换
- 支持makdwon语法的代码编写
- 支持流式输出
- 支持AI绘画
- 支持一键识别图片内容进行交互
- 等等…
- 更多亮点可自己去挖掘

### 工程介绍

|  工程   | 描述  | 地址 |
|  ----  | ----  | ---- |
| chatgpt-template  | plum-gpt前端 | https://gitee.com/susantyp/chatgpt-template |
| chatgpt-template-java  | plum-gpt后端 | https://gitee.com/susantyp/chatgpt-template-java |
| mobile-chatgpt  | plum-gpt手机端 | https://gitee.com/susantyp/mobile-chatgpt |
| chatgpt-platform  | plum-gpt 管理端前端 | https://gitee.com/susantyp/chatgpt-platform |
| chatgpt-platform-java  | plum-gpt 管理端后端 | https://gitee.com/susantyp/chatgpt-platform-java |

### GPT部署详细流程

查看博文地址：https://blog.csdn.net/Susan003/article/details/132207654

`如果嫌部署太麻烦，可加作者微信：typsusan 可让作者帮忙部署项目（白嫖档别加）`

