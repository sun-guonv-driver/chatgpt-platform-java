package com.open.util;

import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import cn.hutool.crypto.symmetric.SymmetricCrypto;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
public class CryptoUtil {

    private final SymmetricCrypto symmetricCrypto;


    public CryptoUtil(String salt) {
        // 使用AES算法，结合自定义盐值来创建对称加密实例
        byte[] key = SecureUtil.generateKey(SymmetricAlgorithm.AES.getValue(), salt.getBytes()).getEncoded();
        symmetricCrypto = new SymmetricCrypto(SymmetricAlgorithm.AES, key);
    }

    // 加密方法
    public String encrypt(String data) {
        return symmetricCrypto.encryptHex(data);
    }

    // 解密方法
    public String decrypt(String data) {
        return symmetricCrypto.decryptStr(data);
    }
}
