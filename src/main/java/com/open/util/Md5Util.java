package com.open.util;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import java.security.Key;
import java.security.SecureRandom;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
public class Md5Util {

    private static Key key;

    private static String KEY_STR = "3edcVFR$";
    private static String CHARSETNAME = "UTF-8";
    private static String ALGORITHM = "DES";


    static {
        try {
            //生成DES算法对象
            KeyGenerator generator=KeyGenerator.getInstance(ALGORITHM);
            //运用SHA1安全策略
            SecureRandom secureRandom=SecureRandom.getInstance("SHA1PRNG");
            //设置上密钥种子
            secureRandom.setSeed(KEY_STR.getBytes());
            //初始化基于SHA1的算法对象
            generator.init(secureRandom);
            //生成密钥对象
            key=generator.generateKey();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    /***
     * 加密
     * @param str
     * @return
     */
    public static String getEncryptString(String str) {
        BASE64Encoder encoder = new BASE64Encoder();
        try {
            byte[] bytes = str.getBytes(CHARSETNAME);
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] doFinal = cipher.doFinal(bytes);
            return encoder.encode(doFinal);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    /***
     * 解密
     * @param str
     * @return
     */
    public static String getDecryptString(String str) {
        BASE64Decoder decoder = new BASE64Decoder();
        try {
            byte[] bytes = decoder.decodeBuffer(str);
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] doFial = cipher.doFinal(bytes);
            return new String(doFial, CHARSETNAME);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
