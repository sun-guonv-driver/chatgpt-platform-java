package com.open.util;

import java.util.Random;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
public class CaptchaGeneratorUtil {

    private static final Random random = new Random();
    private static final String[] SPECIAL_CHARS = {"!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "-", "+", "=", "|", "<", ">", "?"};
    private static final String[] LETTERS = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

    public static String generateCaptcha(int length) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            int type = random.nextInt(3);
            if (type == 0) {
                sb.append(randomDigit());
            } else if (type == 1) {
                sb.append(randomSpecialChar());
            } else {
                sb.append(randomLetter());
            }
        }
        return sb.toString();
    }

    private static char randomDigit() {
        return (char) (random.nextInt(10) + '0');
    }

    private static char randomSpecialChar() {
        return SPECIAL_CHARS[random.nextInt(SPECIAL_CHARS.length)].charAt(0);
    }

    private static char randomLetter() {
        return LETTERS[random.nextInt(LETTERS.length)].charAt(0);
    }

}
