package com.open.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.open.result.OpenResult;
import com.open.service.ReportService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
@RestController
@RequestMapping("/request/open")
@Slf4j
public class AdminReportController {

    @Autowired
    ReportService reportService;

    @GetMapping("/admin/report/query")
    public OpenResult reportQuery(@RequestParam Integer dateType,
                                  @RequestParam(required = false) String dateStr,
                                  @RequestParam Integer value){

        try {
            if (StrUtil.isBlank(dateStr)){
                dateStr = DateUtil.format(new Date(), "yyyy-MM-dd");
            }
            if (dateType == 0){
                return OpenResult.success(reportService.queryHour(dateStr,value,tableName(value),timeName(value)));
            }else if (dateType == 1){
                return OpenResult.success(reportService.queryDay(dateStr,value,tableName(value),timeName(value)));
            }else {
                return OpenResult.success(reportService.queryMonth(dateStr,value,tableName(value),timeName(value)));
            }
        }catch (Exception e){
            return OpenResult.success();
        }
    }

    public String tableName(Integer value){
        String tableName;
        if (value <= 0){
            tableName = "request_record";
        }else if (value == 1){
            tableName = "painting_record";
        }else if (value == 2){
            tableName = "ip_white";
        }else {
            tableName = "login_record";
        }
        return tableName;
    }

    public String timeName(Integer value){
        String timeName;
        if (value <= 0){
            timeName = "request_time";
        }else if (value == 1 || value == 2){
            timeName = "create_time";
        }else {
            timeName = "login_time";
        }
        return timeName;
    }
}
