package com.open.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.open.bean.IpWhite;
import com.open.bean.UserUpgradation;
import com.open.mapper.IpWhiteMapper;
import com.open.result.OpenResult;
import com.open.service.IpWhiteService;
import com.open.service.UserUpgradationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
@RestController
@RequestMapping("/request/open")
@Slf4j
public class AdminUserController {

    @Autowired
    IpWhiteService ipWhiteService;

    @Autowired
    UserUpgradationService userUpgradationService;

    @Autowired
    IpWhiteMapper ipWhiteMapper;

    @GetMapping("/admin/user/list")
    public OpenResult userList(@RequestParam Integer current,
                               @RequestParam Integer size,
                               @RequestParam(required = false) String email,
                               @RequestParam(required = false) String userType,
                               @RequestParam(required = false) String ipStatus){
        IPage<IpWhite> page = ipWhiteService.page(new Page(current,size),new QueryWrapper<IpWhite>()
                .like(StrUtil.isNotBlank(email), "email", email)
                .eq(StrUtil.isNotBlank(userType), "user_type", userType)
                .eq(StrUtil.isNotBlank(ipStatus), "ip_status", ipStatus).orderByDesc("create_time"));
        return OpenResult.success(page);
    }

    @PostMapping("/admin/user/save")
    public OpenResult save(@RequestBody IpWhite ipWhite){
        ipWhite.setIp("127.0.0.1");
        ipWhite.setCreateTime(new Date());
        ipWhite.setUpdateTime(new Date());
        ipWhite.setIpStatus(0);
        ipWhite.setNikeName("ME");
        ipWhite.setRequestImgCount(30);
        ipWhite.setHeadImg("暂无头像");
        ipWhite.setIsImgShow(0);
        if (ipWhiteService.lambdaQuery().eq(IpWhite::getEmail,ipWhite.getEmail()).count() > 0) {
            return OpenResult.error("存在重复的用户");
        }
        int insert = ipWhiteMapper.insert(ipWhite);
        if (ipWhite.getUserType() == 0){
            if (ipWhite.getRequestCount() != 3 || ipWhite.getResettingCount() !=0 || ipWhite.getLimitationCount() != 3){
                upgradation(ipWhite.getWid(),ipWhite.getEmail(),"普通用户注册升级，调整默认额度");
            }
        }else if (ipWhite.getUserType() == 1){
            upgradation(ipWhite.getWid(),ipWhite.getEmail(),"系统指定内测用户");
        }else if (ipWhite.getUserType() == 2){
            upgradation(ipWhite.getWid(),ipWhite.getEmail(),"PLUS用户1注册升级，等级1");
        }else if (ipWhite.getUserType() == 3){
            upgradation(ipWhite.getWid(),ipWhite.getEmail(),"PLUS用户2注册升级，等级2");
        }else if (ipWhite.getUserType() == 4){
            upgradation(ipWhite.getWid(),ipWhite.getEmail(),"PLUS用户3注册升级，等级3");
        }

        return OpenResult.save(insert>0);
    }

    public void upgradation(Integer wid,String username,String msg){
        UserUpgradation upgradation = new UserUpgradation();
        upgradation.setUid(wid);
        upgradation.setUsername(username);
        upgradation.setCreateTime(new Date());
        upgradation.setTypeDes(msg);
    }

    @PostMapping("/admin/user/update")
    public OpenResult update(@RequestBody IpWhite ipWhite){
        IpWhite white = ipWhiteService.getById(ipWhite.getWid());
        if (!white.getUserType().equals(ipWhite.getUserType())){
            upgradation(ipWhite.getWid(),white.getEmail(),"用户类型调整，系统手动调整");
        }
        boolean update = ipWhiteService.lambdaUpdate()
                .set(IpWhite::getUserType, ipWhite.getUserType())
                .set(IpWhite::getRequestCount, ipWhite.getRequestCount())
                .set(IpWhite::getResettingCount, ipWhite.getResettingCount())
                .set(IpWhite::getLimitationCount, ipWhite.getLimitationCount())
                .eq(IpWhite::getWid, ipWhite.getWid()).update();
        return OpenResult.update(update);
    }

    @GetMapping("/admin/user/info/{wid}")
    public OpenResult info(@PathVariable String wid){
        IpWhite ipWhite = ipWhiteService.getById(wid);
        return OpenResult.success(ipWhite);
    }

//    @GetMapping("/admin/user/delete/{wid}")
//    public OpenResult delete(@PathVariable String wid){
//        boolean remove = ipWhiteService.removeById(wid);
//        return OpenResult.delete(remove);
//    }
}
