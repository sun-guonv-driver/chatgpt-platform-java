package com.open.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName ai_draw_server
 */
@TableName(value ="ai_draw_server")
@Data
public class AiDrawServer implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer sid;

    /**
     * 服务器名称
     */
    private String serverName;

    /**
     * 服务器类型，（0=公共服务器，1=个人服务器，3=自建服务器）
     */
    private Integer serverType;

    /**
     * 服务器人数
     */
    private Integer serverCount;

    /**
     * 服务器图标
     */
    private String serverIcon;

    /**
     * 是否可用，0=可用，1=禁用
     */
    private Integer serverStatus;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 创建人
     */
    private Integer wid;

    /**
     * 创建人集合
     */
    private String createWidList;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}