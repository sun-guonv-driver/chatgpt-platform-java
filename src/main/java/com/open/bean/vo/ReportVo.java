package com.open.bean.vo;

import com.open.bean.vo.date.DayVo;
import com.open.bean.vo.date.HourVo;
import com.open.bean.vo.date.MonthVo;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author page-view
 * <p>des</p>
 **/
@Data
public class ReportVo {

    private List<Integer> data;

    private List<HourVo> hourVos;

    public void setHourVos(HourVo hourVos) {
        List<HourVo> hourVoList = new ArrayList<>();
        hourVoList.add(hourVos);
        this.hourVos = hourVoList;
    }

    public void setDayVos(DayVo dayVos) {
        List<DayVo> dayVoList = new ArrayList<>();
        dayVoList.add(dayVos);
        this.dayVos = dayVoList;
    }

    public void setMonthVos(MonthVo monthVos) {
        List<MonthVo> monthVoList = new ArrayList<>();
        monthVoList.add(monthVos);
        this.monthVos = monthVoList;
    }

    private List<DayVo> dayVos;

    private List<MonthVo> monthVos;

}
