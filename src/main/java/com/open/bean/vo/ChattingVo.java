package com.open.bean.vo;

import com.open.bean.RequestRecord;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
@Data
public class ChattingVo {

    private Long groupCode;

    private List<RequestRecordVo> requestRecordVoList = new ArrayList<>();

    public ChattingVo(Long groupCode, List<RequestRecord> requestRecordList) {
        this.groupCode = groupCode;
        for (RequestRecord record : requestRecordList) {
            requestRecordVoList.add(new RequestRecordVo(record.getRequestContent(),record.getRespondContent(),record.getRequestId(),
                    record.getSystemName(),record.getSystemRid(),record.getModelType()));
        }
    }

    public ChattingVo() {
    }
}
