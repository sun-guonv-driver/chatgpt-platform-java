package com.open.bean.vo.date;

import lombok.Data;

/**
 * @author page-view
 * <p>des</p>
 **/
@Data
public class MonthVo {

    private Integer m1;
    private Integer m2;
    private Integer m3;
    private Integer m4;
    private Integer m5;
    private Integer m6;
    private Integer m7;
    private Integer m8;
    private Integer m9;
    private Integer m10;
    private Integer m11;
    private Integer m12;
}
