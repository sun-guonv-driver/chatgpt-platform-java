package com.open.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName painting_record
 */
@TableName(value ="painting_record")
@Data
public class PaintingRecord implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer pid;

    /**
     * 服务器id
     */
    private Integer sid;

    /**
     * 服务器类型，（0=公共服务器，1=个人服务器，3=自建服务器）
     */
    private Integer serverType;

    /**
     * 图片数据
     */
    private String imgBase;

    /**
     * 用户类型
     */
    private Integer userType;

    /**
     * 用户头像
     */
    private String userHeadImg;

    /**
     * 用户昵称
     */
    private String userNickname;

    /**
     * 用户账户
     */
    private String userUsername;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 文字描述
     */
    private String prompt;

    /**
     * 图片细节处理
     */
    private Integer particulars;

    /**
     * 图片制导比例
     */
    private Double guidanceScale;

    /**
     * 图片比率
     */
    private String imgBi;

    /**
     * 否定提示词
     */
    private String negativePrompts;

    /**
     * 采样率
     */
    private String sampler;

    /**
     * 随机种子值
     */
    private String seed;

    /**
     * 去噪的步骤数
     */
    private Integer steps;

    /**
     * 图片的风格配置
     */
    private String style;

    /**
     * 创建人
     */
    private Integer wid;

    /**
     * 是否精选图片，0=否，1=是
     */
    private Integer carefullyImg;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}