package com.open.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName request_record
 */
@TableName(value ="request_record")
@Data
public class RequestRecord implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer requestId;

    /**
     * ip地址区分用户唯一标识
     */
    private String requestIp;

    /**
     * 请求密钥
     */
    private String requestSign;

    /**
     * 0=成功，1=失败
     */
    private Integer requestStatus;

    /**
     * 请求的话语
     */
    private String requestContent;

    /**
     * 请求的时间
     */
    private Date requestTime;

    /**
     * 机器人回复的话术
     */
    private String respondContent;

    /**
     * 机器人回应的时间
     */
    private Date respondTime;

    /**
     * 请求状态消息
     */
    private String requestMsg;

    /**
     * 0否，1是
     */
    private Integer isDelete;

    /**
     * 分组码
     */
    private Long groupCode;

    /**
     * 根据ip转换得到的昵称
     */
    private String nikeName;

    /**
     * 创建人
     */
    private Integer wid;

    /**
     * 使用的是哪个token
     */
    private Integer useToken;

    /**
     * 用户类型
     */
    private Integer userType;

    /**
     * 角色名称
     */
    private String systemName;

    /**
     * 角色值
     */
    private String systemValue;

    /**
     * 角色id
     */
    private Integer systemRid;

    /**
     * 消息来源(pc/mobile)
     */
    private String msgResources;

    /**
     * 消息类型(text/voice)
     */
    private String msgType;

    /**
     * 使用的模型
     */
    private String modelType;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}