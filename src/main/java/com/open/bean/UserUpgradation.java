package com.open.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName user_upgradation
 */
@TableName(value ="user_upgradation")
@Data
public class UserUpgradation implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer upid;

    /**
     * 被升级的人
     */
    private Integer uid;

    /**
     * 被升级的账户
     */
    private String username;

    /**
     * 升级时间
     */
    private Date createTime;

    /**
     * 升级类型描述
     */
    private String typeDes;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}