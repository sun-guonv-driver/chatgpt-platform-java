package com.open.mapper;

import com.open.bean.IpWhite;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author typsusan
* @description 针对表【ip_white】的数据库操作Mapper
* @createDate 2023-06-07 11:26:07
* @Entity com.open.bean.IpWhite
*/
public interface IpWhiteMapper extends BaseMapper<IpWhite> {

}




