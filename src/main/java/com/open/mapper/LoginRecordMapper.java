package com.open.mapper;

import com.open.bean.LoginRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author typsusan
* @description 针对表【login_record(登录记录)】的数据库操作Mapper
* @createDate 2023-06-07 11:26:07
* @Entity com.open.bean.LoginRecord
*/
public interface LoginRecordMapper extends BaseMapper<LoginRecord> {

}




