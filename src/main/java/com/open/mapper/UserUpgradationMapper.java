package com.open.mapper;

import com.open.bean.UserUpgradation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author typsusan
* @description 针对表【user_upgradation】的数据库操作Mapper
* @createDate 2023-06-07 16:33:14
* @Entity com.open.bean.UserUpgradation
*/
public interface UserUpgradationMapper extends BaseMapper<UserUpgradation> {

}




