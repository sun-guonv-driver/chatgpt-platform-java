package com.open.mapper;

import com.open.bean.AdminUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.open.bean.vo.date.DayVo;
import com.open.bean.vo.date.HourVo;
import com.open.bean.vo.date.MonthVo;
import org.apache.ibatis.annotations.Param;

/**
* @author typsusan
* @description 针对表【admin_user】的数据库操作Mapper
* @createDate 2023-06-07 11:26:07
* @Entity com.open.bean.AdminUser
*/
public interface AdminUserMapper extends BaseMapper<AdminUser> {

    HourVo queryHour(@Param("year") String year, @Param("month") String month, @Param("day") String day, @Param("tableName")String tableName, @Param("unit")String unit, @Param("timeName")String timeName);

    DayVo queryDay(@Param("year") String year, @Param("month") String month, @Param("day") String daytimeName, @Param("tableName")String tableName, @Param("unit")String unit, @Param("timeName")String timeName);

    MonthVo queryMonth(@Param("year") String year, @Param("month") String month, @Param("day") String daytimeName, @Param("tableName")String tableName, @Param("unit")String unit, @Param("timeName")String timeName);

}




