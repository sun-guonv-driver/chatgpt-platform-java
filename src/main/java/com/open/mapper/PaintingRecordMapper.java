package com.open.mapper;

import com.open.bean.PaintingRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author typsusan
* @description 针对表【painting_record】的数据库操作Mapper
* @createDate 2023-06-07 11:26:08
* @Entity com.open.bean.PaintingRecord
*/
public interface PaintingRecordMapper extends BaseMapper<PaintingRecord> {

}




