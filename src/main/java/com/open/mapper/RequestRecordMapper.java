package com.open.mapper;

import com.open.bean.RequestRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author typsusan
* @description 针对表【request_record】的数据库操作Mapper
* @createDate 2023-06-07 11:26:08
* @Entity com.open.bean.RequestRecord
*/
public interface RequestRecordMapper extends BaseMapper<RequestRecord> {

}




