package com.open.result;

import lombok.Data;

/**
 * @author page-view
 * <p>des</p>
 **/
@Data
public class OpenResult {

    /**
     * 返回消息
     */
    private String msg;

    /**
     * 状态码
     */
    private int code;

    /**
     * 返回token
     */
    private String token;

    /**
     * 数据集
     */
    private Object data;

    /**
     * 登录人
     */
    private String username;

    /**
     * 用户类型
     */
    private String userType;

    /**
     * 存活时间
     */
    private long userTime;


    /*成功*/
    public static final Integer SUCCESS_CODE = 200;
    /*失败*/
    public static final Integer ERROR = -1;
    /*失效*/
    public static final Integer USER_ERROR = -2;
    /*线路不够异常*/
    public static final Integer READ_ERROR = -3;

    /**
     * 网站错误
     */
    public static final Integer WEB_ERROR = -5;

    /*流量豆余额不足*/
    public static final Integer USER_FLOW_BEAN_ERROR = -4;

    public static final String UPDATE_SUCCESS = "修改成功";
    public static final String UPDATE_ERROR = "修改失败";
    public static final String SAVE_SUCCESS = "添加成功";
    public static final String SAVE_ERROR = "添加失败";
    public static final String DELETE_SUCCESS = "删除成功";
    public static final String DELETE_ERROR = "删除失败";

    public OpenResult setUserType(String userType) {
        this.userType = userType;
        return this;
    }

    public OpenResult setUserTime(long userTime) {
        this.userTime = userTime;
        return this;
    }

    public OpenResult setCode(int code) {
        this.code = code;
        return this;
    }

    public OpenResult setUsername(String username) {
        this.username = username;
        return this;
    }

    public OpenResult setData(Object data) {
        this.data = data;
        return this;
    }

    public OpenResult setToken(String token) {
        this.token = token;
        return this;
    }

    public OpenResult setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public static OpenResult saveSuccess(){
        return new OpenResult().setCode(OpenResult.SUCCESS_CODE).setMsg(OpenResult.SAVE_SUCCESS);
    }
    public static OpenResult saveError(){
        return new OpenResult().setCode(OpenResult.ERROR).setMsg(OpenResult.SAVE_ERROR);
    }

    public static OpenResult save(boolean save){
        return save?saveSuccess():saveError();
    }

    public static OpenResult updateSuccess(){
        return new OpenResult().setCode(OpenResult.SUCCESS_CODE).setMsg(OpenResult.UPDATE_SUCCESS);
    }
    public static OpenResult updateError(){
        return new OpenResult().setCode(OpenResult.ERROR).setMsg(OpenResult.UPDATE_ERROR);
    }

    public static OpenResult update(boolean update){
        return update?updateSuccess():updateError();
    }

    public static OpenResult deleteSuccess(){
        return new OpenResult().setCode(OpenResult.SUCCESS_CODE).setMsg(OpenResult.DELETE_SUCCESS);
    }
    public static OpenResult deleteError(){
        return new OpenResult().setCode(OpenResult.ERROR).setMsg(OpenResult.DELETE_ERROR);
    }

    public static OpenResult delete(boolean delete){
        return delete?deleteSuccess():deleteError();
    }

    public static OpenResult success(int code, String msg,String token){
        return new OpenResult().setCode(code).setMsg(msg).setToken(token);
    }

    public static OpenResult success(int code, String msg,String token,String username){
        return new OpenResult().setCode(code).setMsg(msg).setToken(token).setUsername(username);
    }

    public static OpenResult error(int code, String msg){
        return new OpenResult().setCode(code).setMsg(msg);
    }
    public static OpenResult error(String msg){
        return new OpenResult().setMsg(msg).setCode(OpenResult.ERROR);
    }

    public static OpenResult error(int code){
        return new OpenResult().setCode(code);
    }

    public static OpenResult success(){
        return new OpenResult().setCode(SUCCESS_CODE);
    }
    public static OpenResult success(Object data){
        return new OpenResult().setCode(SUCCESS_CODE).setData(data);
    }
    public static OpenResult success(int code,String msg){
        return new OpenResult().setCode(code).setMsg(msg);
    }
}
