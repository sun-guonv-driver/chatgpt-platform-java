package com.open.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.open.bean.IpWhite;
import com.open.service.IpWhiteService;
import com.open.mapper.IpWhiteMapper;
import org.springframework.stereotype.Service;

/**
* @author typsusan
* @description 针对表【ip_white】的数据库操作Service实现
* @createDate 2023-06-07 11:26:07
*/
@Service
public class IpWhiteServiceImpl extends ServiceImpl<IpWhiteMapper, IpWhite>
    implements IpWhiteService{

}




