package com.open.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.open.bean.OpenSystemRole;
import com.open.service.OpenSystemRoleService;
import com.open.mapper.OpenSystemRoleMapper;
import org.springframework.stereotype.Service;

/**
* @author typsusan
* @description 针对表【open_system_role】的数据库操作Service实现
* @createDate 2023-06-07 11:26:08
*/
@Service
public class OpenSystemRoleServiceImpl extends ServiceImpl<OpenSystemRoleMapper, OpenSystemRole>
    implements OpenSystemRoleService{

}




