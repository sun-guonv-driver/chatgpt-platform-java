package com.open.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.open.bean.DrawImg;
import com.open.service.DrawImgService;
import com.open.mapper.DrawImgMapper;
import org.springframework.stereotype.Service;

/**
* @author typsusan
* @description 针对表【draw_img】的数据库操作Service实现
* @createDate 2023-06-07 11:26:07
*/
@Service
public class DrawImgServiceImpl extends ServiceImpl<DrawImgMapper, DrawImg>
    implements DrawImgService{

}




