package com.open.service;

import com.open.bean.PaintingRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author typsusan
* @description 针对表【painting_record】的数据库操作Service
* @createDate 2023-06-07 11:26:08
*/
public interface PaintingRecordService extends IService<PaintingRecord> {

}
