package com.open.service;

import com.open.bean.UserUpgradation;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author typsusan
* @description 针对表【user_upgradation】的数据库操作Service
* @createDate 2023-06-07 16:33:14
*/
public interface UserUpgradationService extends IService<UserUpgradation> {

}
