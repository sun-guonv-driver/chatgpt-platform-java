package com.open.service;

import com.open.bean.OpenSystemRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author typsusan
* @description 针对表【open_system_role】的数据库操作Service
* @createDate 2023-06-07 11:26:08
*/
public interface OpenSystemRoleService extends IService<OpenSystemRole> {

}
