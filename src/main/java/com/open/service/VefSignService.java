package com.open.service;

import com.open.bean.VefSign;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author typsusan
* @description 针对表【vef_sign】的数据库操作Service
* @createDate 2023-06-07 11:26:08
*/
public interface VefSignService extends IService<VefSign> {

}
