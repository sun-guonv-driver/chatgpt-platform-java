package com.open.service;

import com.open.bean.DrawImg;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author typsusan
* @description 针对表【draw_img】的数据库操作Service
* @createDate 2023-06-07 11:26:07
*/
public interface DrawImgService extends IService<DrawImg> {

}
