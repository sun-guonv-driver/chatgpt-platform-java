package com.open.service;

import com.open.bean.vo.ReportVo;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
public interface ReportService {

    ReportVo queryHour(String dateStr, Integer value, String tableName,String timeName) throws IllegalAccessException;

    ReportVo queryDay(String dateStr,Integer value,String tableName,String timeName) throws IllegalAccessException;

    ReportVo queryMonth(String dateStr,Integer value,String tableName,String timeName) throws IllegalAccessException;

}
